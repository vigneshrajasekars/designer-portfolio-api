package com.designer.portfolio.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.designer.portfolio.model.Branding;
import com.designer.portfolio.model.BrandingType;

@Repository
public interface BrandingJpaRepository extends JpaRepository<Branding, Long> {
	Optional<Branding> findById(Long brandingId);
	List<Branding> findAll();
}
