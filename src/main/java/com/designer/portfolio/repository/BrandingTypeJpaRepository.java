package com.designer.portfolio.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.designer.portfolio.model.BrandingType;

@Repository
public interface BrandingTypeJpaRepository extends JpaRepository<BrandingType, Long> {
	Optional<BrandingType> findById(Long brandingTypeId);
	List<BrandingType> findAll();

}
