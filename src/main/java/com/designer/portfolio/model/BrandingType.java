package com.designer.portfolio.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.designer.portfolio.model.audit.DateAudit;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
public class BrandingType extends DateAudit   {

	public BrandingType() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Branding> getBrandings() {
		return brandings;
	}

	public void addBranding(Branding branding) {
		this.brandings.add(branding);
	}
	
	public void removeBranding(Branding branding) {
		this.brandings.remove(branding);
	}
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 40)
	private String name;
	

	@OneToMany(mappedBy="brandingType", fetch = FetchType.EAGER)
	private List<Branding> brandings = new ArrayList<>();


}
