package com.designer.portfolio.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.designer.portfolio.model.audit.DateAudit;

@Entity
public class IllustrationType extends DateAudit {

	public IllustrationType() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Illustration> getIllustrations() {
		return illustrations;
	}

	
	public void addIllustration(Illustration illustration) {
		this.illustrations.add(illustration);
	}
	
	public void removeIllustration(Illustration illustration) {
		this.illustrations.remove(illustration);
	}
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 40)
	private String name;
	
	@OneToMany(mappedBy="illustrationType")
	private List<Illustration> illustrations = new ArrayList<>();


}
