package com.designer.portfolio.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.designer.portfolio.model.audit.DateAudit;

@Entity
public class AdvertisingType extends DateAudit{

	public AdvertisingType() {
		// TODO Auto-generated constructor stub
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Advertising> getAdvertisings() {
		return advertisings;
	}

	public void addAdvertising(Advertising advertising) {
		this.advertisings.add(advertising);
	}
	
	public void removeAdvertising(Advertising advertising) {
		this.advertisings.remove(advertising);
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 40)
	private String name;
	
	@OneToMany(mappedBy="advertisingType")
	private List<Advertising> advertisings = new ArrayList<>();

}
