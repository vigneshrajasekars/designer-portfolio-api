package com.designer.portfolio.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.designer.portfolio.model.audit.DateAudit;

@Entity
public class MarketingType extends DateAudit {

	public MarketingType() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Marketing> getMarketings() {
		return marketings;
	}

	public void addMarketing(Marketing marketing) {
		this.marketings.add(marketing);
	}

	public void removeMarketing(Marketing marketing) {
		this.marketings.remove(marketing);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 40)
	private String name;
	
	@OneToMany(mappedBy="marketingType")
	private List<Marketing> marketings = new ArrayList<>();

}
