package com.designer.portfolio.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.designer.portfolio.exception.AppException;
import com.designer.portfolio.exception.ResourceNotFoundException;
import com.designer.portfolio.model.BrandingType;
import com.designer.portfolio.payload.ApiResponse;
import com.designer.portfolio.payload.BrandingTypeRequest;
import com.designer.portfolio.payload.BrandingTypeResponse;
import com.designer.portfolio.repository.BrandingTypeRepository;
import com.designer.portfolio.repository.BrandingTypeJpaRepository;
@RestController
@RequestMapping("/api/brandingType")
public class BrandingTypeController {

	
	@Autowired
	private BrandingTypeJpaRepository brandingTypeJpaRepository;
	
	@PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> createBrandingType(@Valid @RequestBody BrandingTypeRequest brandingTypeRequest) {
		
		BrandingType brandingType = new BrandingType();
		brandingType.setName(brandingTypeRequest.getName());
		BrandingType result = brandingTypeJpaRepository.save(brandingType);
        
        

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{brandingTypeId}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "Branding Type Created Successfully"));
    }
	
    @GetMapping("/{brandingTypeId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<BrandingType> getBrandingTypeById(@PathVariable Long brandingTypeId) {
    	BrandingType brandingType = brandingTypeJpaRepository.findById(brandingTypeId)
    			.orElseThrow(() -> new ResourceNotFoundException("Branding Type","id",brandingTypeId));
    	return ResponseEntity.ok().body(brandingType);
    }
    
    @GetMapping
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<List<BrandingType>> getBrandings() {
    	List<BrandingType> brandingTypes = brandingTypeJpaRepository.findAll();
    			//.orElseThrow(() -> new ResourceNotFoundException("Branding Type","id",brandingTypeId));
    	return ResponseEntity.ok().body(brandingTypes);
    }
    
    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateBrandingType(@Valid @RequestBody BrandingTypeRequest brandingTypeRequest) {
    	BrandingType brandingType = brandingTypeJpaRepository.findById(brandingTypeRequest.getId())
    			.orElseThrow(() -> new ResourceNotFoundException("Branding Type","id",brandingTypeRequest.getId()));
		brandingType.setName(brandingTypeRequest.getName());
		BrandingType result = brandingTypeJpaRepository.save(brandingType);
        
        

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{brandingTypeId}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "Branding Type Updated Successfully"));
    }
	
	


}
