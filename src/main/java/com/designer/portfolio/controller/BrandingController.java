package com.designer.portfolio.controller;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

import javax.transaction.Transactional;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.designer.portfolio.exception.ResourceNotFoundException;
import com.designer.portfolio.model.Branding;
import com.designer.portfolio.model.BrandingType;
import com.designer.portfolio.payload.ApiResponse;
import com.designer.portfolio.payload.BrandingRequest;
import com.designer.portfolio.repository.BrandingJpaRepository;
import com.designer.portfolio.repository.BrandingTypeJpaRepository;

@RestController
@RequestMapping("/api/branding")
public class BrandingController {


	@Autowired
	BrandingJpaRepository brandingJpaRepository ;

	@Autowired
	BrandingTypeJpaRepository brandingTypeJpaRepository;

	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	@Transactional
	public ResponseEntity<?> createBrandingType(@Valid @RequestBody BrandingRequest brandingRequest) {

		Branding branding = new Branding();
		branding.setTitle(brandingRequest.getTitle());
		branding.setDescription(brandingRequest.getDescription());
		BrandingType brandingType = brandingTypeJpaRepository.findById(brandingRequest.getBrandingTypeId())
				.orElseThrow(() -> new ResourceNotFoundException("Branding","id",brandingRequest.getBrandingTypeId()));
		// 		branding.setBrandingType(brandingType);
		Branding result = brandingJpaRepository.save(branding);
		result.setBrandingType(brandingType);
		Branding brandingResult = brandingJpaRepository.save(result);

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{brandingId}")
				.buildAndExpand(result.getId()).toUri();

		return ResponseEntity.created(location)
				.body(new ApiResponse(true, "Branding Created Successfully"));
	}

	@GetMapping("/{brandingId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Branding> getBrandingById(@PathVariable Long brandingId) {
		Branding branding = brandingJpaRepository.findById(brandingId)
				.orElseThrow(() -> new ResourceNotFoundException("Branding","id",brandingId));
		return ResponseEntity.ok().body(branding);
	}
	

	
	

	@GetMapping
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	public ResponseEntity<List<Branding>> getBrandings() {
		List<Branding> brandings = brandingJpaRepository.findAll();
		return ResponseEntity.ok().body(brandings);
	}



}
