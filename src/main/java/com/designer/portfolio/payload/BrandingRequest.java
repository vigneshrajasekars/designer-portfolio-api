package com.designer.portfolio.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BrandingRequest {

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getBrandingTypeId() {
		return brandingTypeId;
	}

	public void setBrandingTypeId(long brandingTypeId) {
		this.brandingTypeId = brandingTypeId;
	}

	private Long id;

	@NotBlank
	@Size(max = 40)
	private String title;

	@NotBlank
	@Size(max = 255)
	private String description;
	
	@NotNull
	private long brandingTypeId;
}
