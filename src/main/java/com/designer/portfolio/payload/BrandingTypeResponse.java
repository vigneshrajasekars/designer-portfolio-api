package com.designer.portfolio.payload;

public class BrandingTypeResponse {

	public BrandingTypeResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private Long id;
    private String name;

}
